using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject snakePrefab;
    public GameObject playerPrefab;
    public Text scoreText;
    public Text livesText;
    public Text levelText;

    public GameObject panelMenu;
    public GameObject panelPlay;
    public GameObject panelGameOver;
    public GameObject panelLevelCompleted;

    public static GameManager Instance { get; private set; }

    public enum State { MENU, INIT, PLAY, LEVELCOMPLETED, LOADLEVEL, GAMEOVER }
    State _state;
    public GameObject[] levels;
    GameObject _currentBall;
    GameObject _currentLevel;

    //Levels
    private int _level;
    public int Level
    {
        get { return _level; }
        set
        {
            _level = value;
            levelText.text = "LEVEL: " + _level;
        }
    }

    //Balls
    private int _balls;
    public int Balls
    {
        get { return _balls; }
        set
        {
            _balls = value;
            livesText.text = "LIVES: " + _balls;
        }
    }

    //Scores
    private int _score;
    public int Score
    {
        get { return _score; }
        set
        {
            _score = value;
            scoreText.text = "SCORE: " + _score;
        }
    }

    public void PlayClicked()
    {
        SwitchState(State.INIT);
    }
    void Start()
    {
        Instance = this;
        SwitchState(State.MENU);
    }

    public void SwitchState(State newState)
    {
        EndState();
        BeginState(newState);
    }

    void BeginState(State newState)
    {
        switch (newState)
        {
            case State.MENU:
                panelMenu.SetActive(true);
                break;

            case State.INIT:
                panelPlay.SetActive(true);
                Score = 0;
                Level = 1;
                Balls = 3;
                Instantiate(snakePrefab);
                SwitchState(State.LOADLEVEL);
                break;

            case State.PLAY:
                break;

            case State.LEVELCOMPLETED:
                panelLevelCompleted.SetActive(true); 
                break;

            case State.LOADLEVEL:
                if(Level >= levels.Length)
                {
                    SwitchState(State.GAMEOVER);
                }
                else
                {
                    _currentLevel = Instantiate(levels[Level]);
                    SwitchState(State.PLAY);
                }
                break;

            case State.GAMEOVER:
                panelGameOver.SetActive(false);
                break;
        }
    }

    void Update()
    {
        switch (_state)
        {
            case State.MENU:
                break;

            case State.INIT:
                break;

            case State.PLAY:
                break;

            case State.LEVELCOMPLETED:
                break;

            case State.LOADLEVEL:
                break;

            case State.GAMEOVER:
                break;
        }
    }

    void EndState()
    {
        switch (_state)
        {
            case State.MENU:
                panelMenu.SetActive(false);
                break;

            case State.INIT:
                break;

            case State.PLAY:
                break;

            case State.LEVELCOMPLETED:
                panelLevelCompleted.SetActive(false);
                break;

            case State.LOADLEVEL:
                break;

            case State.GAMEOVER:
                panelPlay.SetActive(false);
                panelGameOver.SetActive(false);
                break;
        }
    }
}
